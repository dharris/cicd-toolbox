# cicd-toolbox

This is a simple and somewhat lightweight CI/CD image based on `alpine`, but with a few useful tools:

1. Curl
1. Jq
1. Bash
1. Python 3
1. Redis
1. Git
1. OpenSSH-client
1. Rsync
1. Shellcheck 
1. jo - https://github.com/jpmens/jo 
1. yq - https://github.com/mikefarah/yq/
